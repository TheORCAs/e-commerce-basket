var express = require('express');
const bodyParser = require('body-parser');
var app = express();
var cors = require("cors");
var indexRouter = require('./routes/index');

//before api call system runs the codes below
//firt we get data from body as a json. it is very important.
app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.raw());

app.use(cors());
//app.use(express.json());
//parameters
const { getCustomerOrdersInBasket } = require('./routes/parameters/getCustomerOrdersInBasket');


//global op.
app.use('/', indexRouter);
//parameters
app.use("/getCustomerOrdersInBasket", getCustomerOrdersInBasket);

var server = app.listen(8081, function () {
  var host = server.address().address
  var port = server.address().port
  console.log("Example app listening at http://%s:%s", host, port)
})
