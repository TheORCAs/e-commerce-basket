const fs = require('fs');
const querystring = require('querystring');

module.exports = {
    getCustomerOrdersInBasket: (req, res) => {
        const orders = [
            { key: 1, label: "Mountain Dew", quantity: 2, salePrice: 1.8 },
            { key: 2, label: "Desperados", quantity: 6, salePrice: 2.583 },
            { key: 3, label: "Jack Daniels", quantity: 4, salePrice: 3.35 },
        ]

        res.send(orders);
    }
};
