import React, { useState, useEffect } from 'react';
import styles from './App.css';
import { ChevronRight, Clear } from '@material-ui/icons';
import { Checkbox } from '@material-ui/core';
import { Button } from "react-bootstrap";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios'
import Modal from 'react-modal';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


function ECommerceApp() {
  const [orderList, setOrderList] = useState([])
  const [showPaymentScreen, setShowPaymentScreen] = useState(0);
  const [totalNetPrice, setTotalNetPrice] = useState(0);
  const [totalVATPrice, setTotalVATPrice] = useState(0);
  const [totalGrossPrice, setTotalGrossPrice] = useState(0);

  let basurl = window.location.hostname;

  useEffect(() => {
    const url = `http://${basurl}:8081/getCustomerOrdersInBasket`
    axios.get(url, {
      params: {
      }
    })
      .then(res => setOrderList(res.data))
  }, [])

  const fetchRowDetails = () => {
    console.log('I got the line')
  }

  const handleClearButton = () => {
    setOrderList(orderList.map((e, i) => orderList[i] = { ...e, quantity: 0 }));
  };

  useEffect(() => {
    setTotalNetPrice(Number.parseFloat(orderList.reduce((total, e) => (e.quantity ? e.quantity * e.salePrice : 0) + total, 0)).toFixed(2));
    setTotalVATPrice(Number.parseFloat(orderList.reduce((total, e) => (e.quantity ? e.quantity * e.salePrice : 0) + total, 0) * 0.2).toFixed(2));
    setTotalGrossPrice(Number.parseFloat(orderList.reduce((total, e) => (e.quantity ? e.quantity * e.salePrice : 0) + total, 0) * 1.2).toFixed(2));

  }, [orderList])

  const submitPayment = () => {
    toast.success("Payment is successful")
    setShowPaymentScreen(0);
  }

  return (
    <div>
      <ToastContainer position="top-center"></ToastContainer>
      <div className="container">
        <table class="table">
          <tbody>
            {orderList.map((e, i) =>
              <tr key={e.key}>
                <td scope="col">{e.label}</td>
                <td scope="col"><input type="text"
                  value={e.quantity}
                  onChange={(b) => setOrderList(orderList.map((e, j) => i === j ? orderList[j] = { ...e, quantity: b.target.value } : orderList[j] = { ...e }))}
                ></input></td>
                <td scope="col">{'\u00A3'} {Number.parseFloat(e.quantity ? e.quantity * e.salePrice : 0).toFixed(2)}  <Clear></Clear></td>
              </tr>)}
          </tbody>
          <tfoot>
            <tr>
              <td scope="col"></td>
              <td scope="col"><Button className="btn btn-warning" onClick={handleClearButton}>Clear</Button></td>
              <td scope="col"><Button className="btn btn-success" onClick={() => setShowPaymentScreen(1)}>Check Out<ChevronRight></ChevronRight></Button></td>
            </tr>
            <tr>
              <td scope="col">{'\u00A3'} {totalNetPrice}</td>
              <td scope="col" colspan="2">Net Total</td>
            </tr>
            <tr>
              <td scope="col">{'\u00A3'} {totalVATPrice}</td>
              <td scope="col" colspan="2">VAT (%20)</td>
            </tr>
            <tr>
              <td scope="col">{'\u00A3'} {totalGrossPrice}</td>
              <td scope="col" colspan="2">Gross Total</td>
            </tr>
          </tfoot>
        </table>

        <Modal
          isOpen={showPaymentScreen}
          onRequestClose={() => setShowPaymentScreen(0)}
          style={{
            overlay: { backgroundColor: "grey" },
            content: { color: "black", height: "80%" }
          }}
        >
          <div>
            <table className="table">
              <tbody>
                <tr>
                  <td scope="col" colspan="2">Paypall</td>
                  <td scope="col"><Checkbox></Checkbox></td>
                </tr>
                <tr>
                  <td scope="col" colspan="2">Visa Credit Card</td>
                  <td scope="col"><Checkbox></Checkbox></td>
                </tr>
                <tr>
                  <td scope="col" colspan="2">Debit Card</td>
                  <td scope="col"><Checkbox></Checkbox></td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td scope="col" colspan="2">Net</td>
                  <td scope="col">{'\u00A3'} {totalNetPrice}</td>
                </tr>
                <tr>
                  <td scope="col" colspan="2">VAT (%20)</td>
                  <td scope="col">{'\u00A3'} {totalVATPrice}</td>
                </tr>
                <tr>
                  <td scope="col" colspan="2">Gross Total</td>
                  <td scope="col">{'\u00A3'} {totalGrossPrice}</td>
                </tr>
                <tr>
                  <td scope="col" colspan="3">
                    <button value="showPaymentScreen" className="btn btn-success" style={{ width: "100%" }} onClick={() => submitPayment()}>Pay</button>
                  </td>
                </tr>
              </tfoot>
            </table>
          </div>
        </Modal>

      </div>
    </div>
  );
}

export default ECommerceApp;
